import React from 'react'

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'

import {AuthProvider} from './Context/authContext'

import Home from './views/Home'
import UserForms from './views/UserForms'

export default function RoutesList() {

    return(
        <AuthProvider>
            <Router>
                <Routes>
                    <Route exact path="/login" element={<UserForms/>}/>
                    <Route exact path="/" element={<Home/>}/>
                </Routes>
            </Router>
            
        </AuthProvider>
        
        )
}
