import React, {useEffect, useContext} from 'react'

import {useNavigate} from 'react-router-dom'

import {AuthContext} from '../../Context/authContext'

function Home() {

    const {user} = useContext(AuthContext)

    let navigate = useNavigate()

    useEffect(()=> {
      console.log(user)
        if(user.login === ""){
            navigate("/login")
        } 
    }, [])

  return (
    <div>
      <h1>
        Logado
      </h1>
    </div>
  )
}

export default Home