import React, {useRef, useContext} from 'react'

import {Form, Title, InputWrapper, SubmitButton, HasAccountButton} from '../styled'

import {AuthContext} from '../../../Context/authContext'

import {useNavigate} from 'react-router-dom'

import {login} from '../services/formsServices'


function LoginForm({setHasAccount}) {
    
    let navigate = useNavigate()

    const loginRef = useRef('')
    const passwordRef = useRef('')

    const {setUser} = useContext(AuthContext)

    const handleSubmit = async () => {

        const res = await login({
            login: loginRef.current.value,
            password: passwordRef.current.value
        })

        if(res.message){
            return
        }

        setUser({login: loginRef.current.value})
        navigate("/") 

    }

    return(
        <Form onSubmit={(e)=> {
            handleSubmit();
            e.preventDefault()
        }}>
            <Title>Login</Title>
            <InputWrapper>            
                <label> Login
                    <input ref={loginRef} />
                </label>
            </InputWrapper>
            <InputWrapper>
                <label> Senha
                    <input type="password" ref={passwordRef} />
                </label>
            </InputWrapper>

            <SubmitButton type="submit"> Entrar</SubmitButton>
            <HasAccountButton onClick={()=> setHasAccount(false)}>Ainda não tem uma conta? Clique aqui!</HasAccountButton>
        </Form>
    )

}

export default LoginForm