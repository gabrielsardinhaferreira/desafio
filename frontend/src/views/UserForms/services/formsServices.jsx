import axios from 'axios'


export const createNewUser = async (data) => {
    const res = await axios.post('http://localhost:8080/criar-login', data)
                .then(res=> res.data)
                .catch(err => console.log(err))
    return res
}

export const login = async (data) => {
    const res = await axios.post('http://localhost:8080/login', data)
                .then(res=> res.data)
                .catch(err=> console.log(err))

    return res
}

export const dataTransaction = async (data) => {
    const res = await axios.post('http://localhost:8080/consultar-historico', data)
                .then(res=> res.data)
                .catch(err=> console.log(err))

    return res
}
