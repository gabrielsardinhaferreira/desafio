import React, {useRef, useContext} from 'react'

import {Form, Title, InputWrapper, SubmitButton, HasAccountButton} from '../styled'

import {AuthContext} from '../../../Context/authContext'

import {useNavigate} from 'react-router-dom'

import {createNewUser} from '../services/formsServices'

function RegisterForm({setHasAccount}) {

    const {setUser} = useContext(AuthContext)

    let navigate = useNavigate()

    const nameRef = useRef('')
    const loginRef = useRef('')
    const passwordRef = useRef('')

    const handleSubmit = async () => {

        const res = await createNewUser({
            nome: nameRef.current.value,
            login: loginRef.current.value,
            senha: passwordRef.current.value
        })

        if(res.message){
            return
        }
        setUser({login: loginRef.current.value})
        navigate('/')
    }

    return(
        <Form onSubmit={(e)=>{
                handleSubmit();
                e.preventDefault()
            }}>
            <Title>Cadastro</Title>
            <InputWrapper>
                <label>Nome
                    <input name="nameField" ref={nameRef} placeholder="Insira seu nome..." />
                </label>
            </InputWrapper>
            <InputWrapper>            
                <label> Login
                    <input ref={loginRef} placeholder="Insira seu login..."/>
                </label>
            </InputWrapper>
            <InputWrapper>
                <label> Senha
                    <input type="password" ref={passwordRef} placeholder="Insira sua senha..." />
                </label>
            </InputWrapper>

            <SubmitButton type="submit">Criar conta</SubmitButton>
            <HasAccountButton onClick={()=> setHasAccount(true)}>Ja tem uma conta? Clique aqui!</HasAccountButton>
        </Form>

            
    )

}

export default RegisterForm