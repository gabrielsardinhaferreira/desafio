import React, {useState} from 'react'

import LoginForm from './LoginForm'
import RegisterForm from './RegisterForm'

import {FormCard} from './styled'

function UserForms() {

  const [hasAccount, setHasAccount] = useState(false)

  return (
    <FormCard>
      {
        hasAccount ? 
          <LoginForm setHasAccount={setHasAccount}/>  
          :
          <RegisterForm setHasAccount={setHasAccount}/>
      }
    </FormCard>
  )
}

export default UserForms