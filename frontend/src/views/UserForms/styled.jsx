import styled from "styled-components";


export const FormCard = styled.div`
    width: 25%;
    height: 80vh;

    background-color: white;
    border-radius: 10px;

    margin: 75px auto;
    padding: 50px 20px 0;
`

export const Form = styled.form`
    display: flex;
    flex-direction: column;
    gap: 20px;

`

export const Title = styled.h1`
    padding: 20px 0;
    text-transform: uppercase;
    text-align: center;
`

export const InputWrapper = styled.div`
    label {
        display:flex;
        flex-direction: column;
        gap: 5px;

    }


    input {
        width: 100%;
        padding: 10px 10px;
        border-radius: 3px;
        border: 1px solid black;
    }
`
export const SubmitButton = styled.button`
    width: 100%;
    background-color: rgba(var(--primary), 0.8);
    transition: .2s linear;
    text-transform: uppercase;
    cursor: pointer;
    padding: 7px 10px;
    border: 1px solid transparent;
    color: white;
    font-size: 16px;
    border-radius: 3px;

    :hover{
        background-color: rgb(var(--primary));
    }
`

export const HasAccountButton = styled.button`
    border: none;
    background-color: transparent;
    cursor: pointer;
`