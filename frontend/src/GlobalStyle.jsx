import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
    *{
        padding: 0;
        margin: 0;
        box-sizing: border-box;
        font-family: 'Roboto', sans-serif;
    }

    :root{
        --primary: 40, 53, 147; 
    }
    
    body{
        background-color: rgb(235, 235, 245)
    }

`