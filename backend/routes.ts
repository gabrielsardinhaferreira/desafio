import { Router } from "express";
import  loginController  from "./src/controller/LoginController"
import  transferLogController  from "./src/controller/TransferLogController"
import  createLoginController  from "./src/controller/CreateLoginController"
import  transferController  from "./src/controller/TransferController"


const router = Router();


router.post("/login", loginController);
router.post("/criar-login", createLoginController);
router.get("/saldo/:login")
router.get("/consultar-historico/:login", transferLogController);
router.post("/transferir/", transferController);


export { router };