import { Request, Response } from "express";
import { prismaClient } from "../database/prismaClient";

const createLoginController = async (request: Request, response: Response) => {
    const {login, senha, nome } = request.body;
    console.log(request.body);
    
    await prismaClient.usuario.create({
        data: {
            login,
            senha,
            nome
        }
    });

    await prismaClient.saldo.create({
        data: {
            login,
            saldo: 100
        }
    });
    return response.json(login)
}

export default createLoginController;
