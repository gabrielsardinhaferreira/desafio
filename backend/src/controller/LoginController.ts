import { Request, response, Response } from "express";
import { prismaClient } from "../database/prismaClient";

const loginController = async (request: Request, response: Response) => {
    const {login, senha} = request.body
     
    const user = await prismaClient.usuario.findFirst({
        where : {
            login,
            senha
        }
    })
    if(user){
        console.log(user);
        return response.status(200).json()
    }
    return response.status(400).json({message: "Não existe"})
    
}

export default loginController;
