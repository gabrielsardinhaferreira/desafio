import { Request, Response } from "express";
import { prismaClient } from "../database/prismaClient";


const transferLogController = async (request: Request, response: Response) => {
    const {login} = request.body
    const movimentacao = await prismaClient.movimentacao.findMany({
        where: {
            login_origin: login
        }
    })

    return response.json(movimentacao);
}

export default transferLogController;