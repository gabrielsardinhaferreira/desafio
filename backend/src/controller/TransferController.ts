import { Request, Response } from "express";
import { prismaClient } from "../database/prismaClient";


const transferController = async (request: Request, response: Response) => {
    const {login_origin, login_destino, valor_transferido} = request.body;
    
    await prismaClient.saldo.update({
        where: {
            login: login_origin
        },
        data: {
            saldo: {
                decrement: valor_transferido
            }
        }
    })

    await prismaClient.saldo.update({
        where: {
            login: login_destino
        },
        data: {
            saldo: {
                increment: valor_transferido
            }
        }
    })
    
    await prismaClient.movimentacao.create({
        data: {
            login_origin,
            login_destino,
            valor_transferido
        }
    })
}



export default transferController