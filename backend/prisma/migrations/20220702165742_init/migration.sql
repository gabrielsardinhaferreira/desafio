-- CreateTable
CREATE TABLE "Usuario" (
    "login" TEXT NOT NULL,
    "senha" TEXT NOT NULL,
    "nome" TEXT NOT NULL,

    CONSTRAINT "Usuario_pkey" PRIMARY KEY ("login")
);

-- CreateTable
CREATE TABLE "Saldo" (
    "login" TEXT NOT NULL,
    "saldo" DOUBLE PRECISION NOT NULL,

    CONSTRAINT "Saldo_pkey" PRIMARY KEY ("login")
);

-- CreateTable
CREATE TABLE "Movimentacao" (
    "id_transacao" SERIAL NOT NULL,
    "data" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "login_origin" TEXT NOT NULL,
    "login_destino" TEXT NOT NULL,
    "valor_transferido" DOUBLE PRECISION NOT NULL,

    CONSTRAINT "Movimentacao_pkey" PRIMARY KEY ("id_transacao")
);
