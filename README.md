**BackEnd(sim) | FrontEnd(sim) -** Acesse sua conta com um login e senha<br/>
**BackEnd(sim) | FrontEnd(não) -** Verifique um saldo na tela inicial<br/>
**BackEnd(sim) | FrontEnd(não) -** Tenha a opção de ver o histórico de transações de entrada e saida (debito e crédito) de dinheiro da sua conta<br/>
**BackEnd(sim) | FrontEnd(não) -** Consiga transferir créditos para a conta de um amigo, usando o nome de usuário/login dele<br/>
**BackEnd(sim) | FrontEnd(sim) -** Deve ser possível criar um novo cadastro, em um formulário com login (obrigatório) senha (obrigatório) e nome (opcional)<br/>
**BackEnd(sim) | FrontEnd(sim) -** Quando uma nova conta for criada, deve ser creditado um valor de 100 reais.<br/>
    Deve haver esse registro na tabela SALDO (abaixo), sem uma movimentação correspondente.<br/>
**BackEnd(sim) | FrontEnd(não) -** Toda transferência para um amigo deve deduzir do saldo da conta de origem e creditar o valor na conta de destino, além de registrar a transferência na tabela MOVIMENTACAO.<br/>

**Extra:**<br/>
**(Para o banco e o pgadmin é utilizado o docker) -** A subida da aplicação ocorrer utilizando docker.<br/>
**(Foi usado o prisma orm no backend, ao iniciar o backend é feito o migration automaticamente e são criadas as tabelas) -** A inicialização das tabelas do banco ser automatizada e não depender de uma pessoa acessar e executar CREATE TABLE e outros comandos similares para preparar o ambiente.<br/>
**(não realizado) -** A senha estar encriptada/com hash no banco usando SHA-256 ou algoritmo similar ou superior que impeça a leitura dela.<br/>

**Super Extra:**<br/>
**(não realizado - criada uma maquina EC2 na amazon, feita todas as instalções, porém não consegui deixar funcional um docker do frontend e do backend) -** Se além do repositório, o ambiente for entregue em algum lugar online já executando para avaliação.<br/>
    Nesse caso devem ser fornecidas instruções e credenciais para acesso ao banco para examinação<br/>
    Não é necessário utilizar um DNS nesse caso. Basta apresentar um IP:PORTA que permita acesso.<br/>

**Execução:**<br/>
Para inciar o banco de dados é necessário ter o docker-compose instalado e executar o comando "docker compose-up" na pasta raiz.<br/>
Na pasta backend está o arquivo .env que contém a conecção com o banco de dados - login: admin | senha: admin | schema: desafio.<br/>
Para executar o backend e o frontend é necessário estar em suas respectivas pastas e executar o comando "yarn dev".<br/>
Os endpoints estão no arquivo router.ts do backend, e são:<br/>
"/login" - POST<br/>
"/criar-login" - POST<br/>
"/saldo/:login" - GET<br/>
"/consultar-historico/:login" - GET<br/>
"/transferir/" - POST<br/>


